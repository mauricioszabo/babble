(ns babble.core-test
  {:babble/config {:presets ["babel-preset-solid"]}}
  (:require [babble.core :as babble]
            [clojure.string :as str :refer [replace]]
            ["solid-js" :as solid]
            ["solid-js/web" :as solid-web]))

(def a "foobar"
  #_
  (babble/transform "<div>10</div>" {:mappings {"solid-js/web" solid-web}
                                     :presets ["babel-preset-solid"]}))

; (macroexpand-1 '(js* "~{}" a))
; (str "Foo" (js* "~{}" a))
; ;

(defn counter-elem []
  (let [[counter set-counter] (solid/createSignal 3)
        help "Help!"]
    #_
    (babble/jsx {:mappings {"solid-js/web" solid-web}
                 :presets ["babel-preset-solid"]}
                [:button {:onClick (fn []
                                     (set-counter (inc (counter))))}
                 "WAT? "
                 counter])
    #_
    (babble/transform "<div>10</div>" {:mappings {"solid-js/web" solid-web}
                                       :presets ["babel-preset-solid"]})))
   ; (js* "<button onClick={~{}}>{~{}()}</button>"
   ;      #(do
   ;         (prn :CLICKED (counter))
   ;         (set-counter (inc (counter))))
   ;      counter)))

; (def foo 100)
; (def jsx (babble/jsx {:mappings {"solid-js/web" solid-web}
;                       :presets ["babel-preset-solid"]}
;                      [:div 10]))
; (js* "\"\"" foo)
; (js* "~{}" ("foo"))

#_#_
(let [elem (js/document.querySelector "#example")]
    ; (set! (.-innerHTML elem) "")
    ; (js/setTimeout)
    (solid-web/render counter-elem elem))
(def ^:dev/after-load reload
  (let [elem (js/document.querySelector "#example")]
    ; (set! (.-innerHTML elem) "")
    ; (js/setTimeout)
    (solid-web/render counter-elem elem)))
(defn index []
  (prn :HELLO)
  (let [elem (js/document.querySelector "#example")]
    (solid-web/render counter-elem elem)))
