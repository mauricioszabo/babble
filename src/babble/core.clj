(ns babble.core
  (:require [clojure.walk :as walk]
            [clojure.string :as str]
            [shadow.cljs.devtools.api :as shadow-api]
            [cljs.compiler :as compiler])
  (:import [com.caoccao.javet.interop NodeRuntime V8Host]))

(defonce runtime (.. V8Host getNodeInstance createV8Runtime))

(defn- transform* [code options]
  (.. runtime getGlobalObject (set "code", code));
  (.. runtime getGlobalObject (set "opts" (dissoc (walk/stringify-keys options)
                                                  "additional-params"
                                                  "override-return"
                                                  "mappings")))
  (.. runtime getGlobalObject (set "additionalParams" (-> options :additional-params keys vec)))
  (.. runtime getGlobalObject (set "overrideReturn" (-> options :override-return)))
  (let [transpiled (.. runtime
                        (getExecutor "(() => {
  opts.ast = true
  opts.code = false
  const babel = require('@babel/core')
  const bt = require('@babel/types')
  const ast = babel.transformSync(code, opts).ast
  let body = [...ast.program.body]
  let imports = {}
  body.forEach((row, i) => {
    if(row.type === 'ImportDeclaration') {
      row.specifiers.forEach(specifier => {
        imports[specifier.local.name] = [row.source.value, specifier.imported.name]
      })
      body[i] = bt.expressionStatement(bt.nullLiteral())
    }
  })
  if(overrideReturn) {
    body.push(bt.returnStatement(bt.identifier(overrideReturn)))
  } else {
    body[body.length-1] = bt.returnStatement(body[body.length-1].expression)
  }
  const params = [...additionalParams, ...Object.keys(imports)]
    .sort()
    .map(i => bt.identifier(i))
  const transpiled = babel.transformFromAstSync(bt.program([
    bt.expressionStatement(
      bt.callExpression(
        bt.functionExpression(
          null, params, bt.blockStatement(body)
        ),
        []
      )
    )
  ])).code
  return [transpiled, imports]
})()")
                        executeObject)]
    {:code (first transpiled)
     :imports (second transpiled)}))

(defmacro transform [code options]
  (let [{:keys [code imports]} (transform* code options)
        mappings-from-env (->> &env
                               :ns
                               :js-deps
                               (map (fn [[k v]] [k (:as v)]))
                               (into {}))
        mappings (merge mappings-from-env (:mappings options))
        all-params (merge (:additional-params options) imports)
        orphan-imports (atom []) ;; Sorry, side-effect because it's easier.
        to-add (for [alias (-> all-params keys sort)
                     :let [param (get all-params alias)]]
                 (if (instance? java.util.ArrayList param)
                   (if-let [import (and (-> param count (= 2))
                                        (get mappings (first param)))]
                     (list '. import (symbol (str "-" (second param))))
                     (swap! orphan-imports conj (first param)))
                   param))
        final-code (str/replace-first code #"\);?$"
                                      (str (str/join ", "
                                                     (repeat (count to-add) "~{}"))
                                           ");"))
        to-add (doall to-add)]

    (when-let [errors (-> @orphan-imports distinct seq)]
      (throw (ex-info (apply str
                        "Not all imports have a mapping for them. Please either"
                        " include them on the namespace require, or add a"
                        " :mappings key with their mappings: "
                        (interpose ", " errors))
                      {:imports (vec errors)})))
    `(~'js* ~final-code ~@to-add)))

(declare ->jsx)
(defn as-map [object js-map?]
  (if (seq object)
    (let [full-result
          (reduce (fn [acc [k v]]
                    (let [key (cond
                                (string? k) k
                                (keyword? k) (name k)
                                :else (pr-str k))
                          [as-str & rest] (->jsx v)
                          as-str (if (and (not js-map?)
                                          (= as-str "~{}"))
                                   "{~{}}"
                                   as-str)]
                      (apply conj
                        (update acc
                                0 str
                                (if js-map? "," " ")
                                key
                                (if js-map? ":" "=")
                                as-str)
                        rest)))
                  [{:code ""}]
                  object)
          without-trailing (update full-result 0 subs 1)]
      (if js-map?
        (update without-trailing 0 #(str "{" % "}"))
        without-trailing))
    [""]))

(defn- to-key [k]
  (cond
    (string? k) k
    (keyword? k) (name k)
    :else (pr-str k)))

(defn- as-map [object]
  (let [result
        (reduce (fn [acc [k v]]
                  (merge-with #(str %1 "," (to-key k) ":" %2) acc (->jsx v)))
                {:code ""}
                object)]
    (update result :code
            #(str "{"
                  (cond-> % (seq %) (subs 1))
                  "}"))))

(defn- as-params [object]
  (let [result
        (reduce (fn [acc [k v]]
                  (let [val (->jsx v)
                        val (if (string? v)
                              val
                              (update val :code #(str "{" % "}")))]
                    (merge-with #(str %1 " " (to-key k) "=" %2) acc val)))
                {:code ""}
                object)]
    (update result :code
            #(cond-> % (seq %) (subs 1)))))

(defn- wrap-obj [object]
  (let [jsx (->jsx object)]
    (if (-> jsx count (> 1))
      (update jsx :code #(str "{" % "}"))
      jsx)))

(defn- vector->jsx [[elem & children]]
  (let [[params body] (if (map? (first children))
                        [(first children) (rest children)]
                        [{} children])
        params (as-params params)
        body (reduce (fn [acc item]
                       (merge-with #(str %1 " " %2) acc (wrap-obj item)))
                     {:code ""}
                     body)
        elem-name (if (keyword? elem)
                    {:code (name elem)}
                    (->jsx elem))]
    (-> params
        (merge body elem-name)
        (assoc :code
               (str "<"
                    (:code elem-name)
                    (when-let [p (-> params :code not-empty)]
                      (str " " p))
                    ">"
                    (:code body)
                    "</"
                    (:code elem-name)
                    ">")))))

(defn- ->jsx [object]
  (cond
    (vector? object) (vector->jsx object)
    (map? object) (as-map object)
    (keyword? object) {:code (name object)}
    (string? object) {:code object}

    (and (list? object) (-> object meta :delay))
    (let [ident (-> "__generated$" gensym str)]
      {:code (str ident "()")
       ident `(fn [] ~object)})

    (or (number? object)
        (boolean? object)
        (nil? object))
    {:code (pr-str object)}

    (symbol? object)
    (let [ident (-> "__generated$" (str (munge object)))]
      {:code ident
       ident object})

    :else
    (let [ident (-> "__generated$" gensym str)]
      {:code ident
       ident object})))

(defmacro jsx [options jsx]
  (let [parsed (->jsx jsx)
        weird-sym (str (gensym "__jsx$"))
        parsed (update parsed :code
                       #(str "const " weird-sym "=" % ";"))]
    `(transform ~(:code parsed) ~(assoc options
                                        :override-return weird-sym
                                        :additional-params (dissoc parsed :code)))))
