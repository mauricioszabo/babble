# Babble

Babbel, but for ClojureScript.

## Usage:

There are two macros (for now): `transform` and `jsx`.

`transform` is _exactly the same_ as Babel's `transform` - it accepts a string
as its first argument, and a Babel config as second argument, and it'll generate
Javascript that is the transformed version of that code (kinda).

`jsx` is the same as `transform`, except the arguments are in reverse and the
_second argument_ can be a hiccup data structure that will be used to generate
JSX (the "pattern" coined from Javascript) and it'll call `transform` on it
to generate a valid JS code.

### Non-babel arguments

#### TL;DR;

Babble accepts `:mappings`, `:additional-params` and `:override-return` as
additional options both for `jsx` and `transform`

* **mappings** is a key-value of strings (that are JS libraries imported via
`import`) to symbols (Clojure symbols that provide that library).
* **additional-params** is because while the `js*` special form allows us to
"escape" Javascript strings and pass Clojure values, neither `transform` nor
`jsx` allow for the same. So, `additional-params` is a map of strings (the
Javascript names that will be available) to their specific values (that can be
Clojure symbols, regexes, etc)
* **override-return** is to not return the _last_ argument of the transformed
Javascript code, but instead return some arbitrary variable. It must be a string

#### Long, explained version

These variables exist because Babel expects to transform _a whole file_
instead of a fragment - that can mean a single command/variable/fragment (like a JSX with `babel-plugin-jsx-dom-expressions` plugin) can generate multiple
statements.

So, imagining that you want to transform `<div>10</div>` to some JSX elements
by using the `{:plugins ["jsx-dom-expressions"]}` options. To make it happen,
we can use the following code:

```clojure
(ns some.namespace
  (:require [babble.core :as babble]))

(def element
  (babble/transform "<div>10</div>" {:plugins ["jsx-dom-expressions"]}))
```

This will generate the following Javascript:

```js
some.namespace.element = import { template as _$template } from "dom";
const _tmpl$ = /*#__PURE__*/_$template(`<div>10`);
_tmpl$();
```

Which is, obviously, not what we want. So Babble wraps around your code into
anonymous functions.

Which, obviously, also doesn't work, because the `import` can't happen anywhere
except on the top-level. So, there are options to control how to translate
imports, what is returned, and other stuff.

##### mappings

By default, `babble` will inspect the namespace to check if you have a JS import
with the same name as what's expected; if you don't, you can add a mapping. So
these all work for our example:

```clojure
(ns some.namespace
  (:require [babble.core :as babble]
            ;; Adding this might "just work":
            ["dom" :as d]))

(def element
  (babble/transform "<div>10</div>" {:plugins ["jsx-dom-expressions"]}))

;;; OR
(def alternate-dom d)
(def element
  (babble/transform "<div>10</div>" {:plugins ["jsx-dom-expressions"]
                                     :mappings {"dom" alternate-dom}}))
```

This code will basically control how the anonymous function that wraps the
compiled Babel code is generated - it'll literally pass the aliases that were
supposed to be imported as parameters to the anonymous function, so the
generated code will be like this (assuming we tried the second alternative):

```js
some.namespace.element = (function(_$template) {
const _tmpl$ = /*#__PURE__*/_$template(`<div>10`);
return _tmpl$();
})(some.namespace.alternate_dom.template);
```

##### additional-mappings

In ClojureScript, we can "escape" some limitations of ClojureScript (like
unsupported language features like early returns, async iterators, async/await
and classes) using the `js*` special form. With this, we can also interpolate ClojureScript code, like `(js* "~{} + ~{}", 10, 30)` - that will return `40`.

In Babble, while _in theory_ we could use the same trick, it won't work - most
transformers will try to transform `~{}` into something, and then the final
`js*` form that Babble generates will be invalid.

To solve this, a way is to pass more arguments to the anonymous function. So
we can use `:additional-mappings` and map any ClojureString data to Javascript.
So, if we add `:additional-mappings {"hello" some-var-in-namespace}` to our
previous example, the generated Javascript will be:

```js
some.namespace.element = (function(_$template, hello) {
const _tmpl$ = /*#__PURE__*/_$template(`<div>10`);
return _tmpl$();
})(some.namespace.alternate_dom.template, some.namespace.some_var_in_namespace);
```

##### override-return

Finally, sometimes a Babel plug-in might generate some additional code **after**
the data we're supposed to be returning. A simple example is to try change our JSX to have a `onClick` handler:

```clojure
(def element
  (babble/transform "<div onClick{doThings}>10</div>"
                    {:plugins ["jsx-dom-expressions"]
                     :mappings {"dom" alternate-dom}}))
```

This will generate this huge code:

```js
some.namespace.element = (function (_$addEventListener, _$delegateEvents, _$template) {
  const _tmpl$ = /*#__PURE__*/_$template(`<div>10`);
  (() => {
    const _el$ = _tmpl$();
    _$addEventListener(_el$, "click", doStuff, true);
    return _el$;
  })();
  return _$delegateEvents(["click"]);
})(
  some.namespace.alternate_dom.addEventListener,
  some.namespace.alternate_dom.delegateEvents,
  some.namespace.alternate_dom.template
);;
```

See how the `return` is _not returning_ the element we want: the place that
generates the element is the anonymous function, one step previous than what is
being returned. The key to make it work is to give a Javascript name, and then
explaining Babbel that we want to return that name. We do that with
`:override-return`, so to make the code work we need to change both the JSX code
_and_ add a parameter to `transform`:

```clojure
;;                 vvvvvvv - We need to add this
(babble/transform "const r = <div onClick={doStuff}>10</div>"
                  {:plugins ["jsx-dom-expressions"]
                   ;; And add this options
                   :override-return "r"
                   :mappings {"dom" alternate-dom}})
```

This will generatew the following Javascript:

```js
some.namespace.element = (function (_$addEventListener, _$delegateEvents, _$template) {
  const _tmpl$ = /*#__PURE__*/_$template(`<div>10`);
  const r = (() => {
    const _el$ = _tmpl$();
    _$addEventListener(_el$, "click", doStuff, true);
    return _el$;
  })();
  _$delegateEvents(["click"]);
  return r;
})(
  some.namespace.alternate_dom.addEventListener,
  some.namespace.alternate_dom.delegateEvents,
  some.namespace.alternate_dom.template
);;
```

The reason it works is because when we added the `const r =` to the beginning of
the Javascript code, the transformed code will also generate the same variable;
then, the `override-return` will not try to _infer_ what is the return code, but
instead add that code to the final Javascript code. This _might generate_
invalid Javascript code, so be sure that you know that variable exist

## Concerns, and things to be aware of

The code below uses `babel-plugin-minify-mangle-names` to make the source smaller:

```clojure
(babble/transform
 "
(function () {
  const someName = 'Me';
  const greeting = 'Hello';
  return `${greeting} ${someName}`;
})()"
 {:plugins ["minify-mangle-names"]})
```

This macro will be compiled to the following JS:

```js
(function () {
  return function () {
    const a = 'Me';
    const b = 'Hello';
    return `${b} ${a}`;
  }();
})();;
```

If that doesn't seem right, it's because it almost isn't; Babel have two ways to
transform code, in which one it returns an AST, and the other it returns the
code; for some reason, if we capture an AST and transform it to code, it doesn't
return the _exact same code_ than it was - unfortunately, not much can be done
in this situation - Babble _needs to use the AST_ instead of the code to support
arbitrary transformations like removing imports, converting things, wrapping
everything around a function and making the last command the return statement
(or allowing the user to configure the return variable). Without these, the code
generated by Babble would most certainly not work when inserted in the middle of
a ClojureScript code.

Also, please be aware that both `transform` and `jsx` are **macros** - they
can't be functions because they need to run through `babel` _in compile time_
otherwise we would need users to have Babel installed, which is far from ideal.
That means, the first argument to `transform` needs to be a string _literal_ -
it can't be, for example, `(str "some string here")`, nor a local/global
variable.

The same is true for `jsx` (and probably a bit more extreme in this case) - we
can't have `(babble/jsx some-hiccup)` because that would be interpreted as a
symbol, to be interpolated in the end result, and we would get just a normal
Clojure vector as a result, not a compiled JSX in the best case (the worst case
could be a bug). This also means that if we need to pass a parameters, like
`[:div {class "hello"}]`, we _need it to be_ a map literal - it can't be a
parameter.

## Stability

This project is quite new, based on a crazy idea I had once. It was completely
altered from its original idea (that was to use GraalVM's Node implementation to
run Babel) and I found lots of problems in the meantime; I know there are things
that still don't work, and some babel code can be nonsense.

That being said - use at your own risk, and report bugs if something weird
happens!
